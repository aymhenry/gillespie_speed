from random import random
from math import log
import time
from numba import jit,float64

@jit(float64(float64,float64,float64))
def activ(x,k,n):
    X=(x/k)**n
    return X/(1+X)
@jit(float64(float64,float64,float64))
def repress(x,k,n):
    X=(x/k)**n
    return 1/(1+X)
@jit
def update_rates(conc):
    rates = [0]*6
    rates[0] = 100*activ(conc[2],50,2)*repress(conc[1],50,2)
    rates[1] = 100*activ(conc[0],50,2)*repress(conc[2],50,2)
    rates[2] = 100*activ(conc[1],50,2)*repress(conc[0],50,2)
    rates[3] = 0.005*conc[0]
    rates[4] = 0.005*conc[1]
    rates[5] = 0.005*conc[2]
    return rates

@jit
def run_gillespie():
    stoichiometry = [[1,0,0],[0,1,0],[0,0,1],[-1,0,0],[0,-1,0],[0,0,-1]]
    conc = [100,50,50]
    results = [list(conc)]
    t = 0
    tMax = 1000
    times = [t]
    while t < tMax:
        rates = update_rates(conc)
        a0 = sum(rates)
        rand1 = random()
        dt = 1/a0*log(1/rand1)
        rand2 = random()
        tot = 0
        rindex = 0
        for j in range(len(rates)):
            tot+= rates[j]/a0
            if rand2<tot:
                rindex = j
                break
        for i in range(len(conc)):
            conc[i] += stoichiometry[rindex][i]
            conc[i] = max(0,conc[i])            
        t+=dt
        times.append(t)
        results.append(list(conc))
    return times,results
#times,conc = run_gillespie()

# import numpy as np
# import matplotlib.pyplot as plt
# conc = np.array(conc)
# fig,ax = plt.subplots()
# for i in range(conc.shape[1]):
#     ax.plot(times,conc[:,i],label="G%d"%i)
# ax.set_xlabel("time")
# ax.set_ylabel("Quantity")
# ax.legend()
# plt.show()


#random.seed(1)
#print(*["%.4f"%random() for i in range(10)])
if __name__ == "__main__":
    run_gillespie()
    t1 = time.clock()
    run_gillespie()
    t2 = time.clock()
    print("Time numba =\t\t %.6f"%(t2-t1))
