#include <cmath>
#include <stdlib.h>
#include <stdio.h>
#include <ctime>

#define R 6
#define C 3

double fRand()
{
  double f = (double)rand() / RAND_MAX;
  return f;
}


double activ(double x,double k,double n)
{
  x=pow(x/k,n);
  return x/(1+x);
}      
double repress(double x,double k,double n)
{
  x=pow(x/k,n);
  return 1/(1+x);
}

void update_rates(const double *conc,double *rates)
{
  rates[0] = 100*activ(conc[2],50.0,2.0)*repress(conc[1],50.0,2.0);
  rates[1] = 100*activ(conc[0],50.0,2.0)*repress(conc[2],50.0,2.0);
  rates[2] = 100*activ(conc[1],50.0,2.0)*repress(conc[0],50.0,2.0);
  
  rates[3] = 0.005*conc[0];  
  rates[4] = 0.005*conc[1];
  rates[5] = 0.005*conc[2];
}



void run_gillespie()
{
  double stoichiometry[6][3] = {{1,0,0},{0,1,0},{0,0,1},{-1,0,0},{0,-1,0},{0,0,-1}};
  double conc[3] = {100,50,50};
  double rates[6] = {0,0,0,0,0,0};
  double dt=0,t=0,tMax=1000;
  double rand1,rand2;
  double tot,a0;
  int i,j,rindex=0;
  //FILE *f = fopen("data.csv", "w");

  //fprintf(f, "%f %f %f %f\n", t,conc[0],conc[1],conc[2]);
  while(t<tMax)
    {

      update_rates(conc,rates);
      a0=0;
      for(i=0;i<R;i++) a0+=rates[i];
      rand1 = fRand();
      dt = 1/a0*std::log(1/rand1);
      rand2 = fRand();
      tot = 0;

      for(j=0;j<R;j++)
	{
	  tot+= rates[j]/a0;
	  if(rand2<tot)
	    {
	      rindex=j;
	      break;
	    }	    
	}
      for(i=0;i<C;i++)
	{
	  conc[i] += stoichiometry[rindex][i];
	  if(conc[i]<0)conc[i]=0;
	}
      t+=dt;
   
      //fprintf(f, "%f %f %f %f\n", t,conc[0],conc[1],conc[2]);
    }
  //fclose(f);
}

int main(){
  std::clock_t t1,t2;
  t1 = std::clock();
  run_gillespie();
  t2 = std::clock();
  printf("Time C++ =\t\t %f\n",((double) (t2-t1)) / CLOCKS_PER_SEC);
  return 0;
}
