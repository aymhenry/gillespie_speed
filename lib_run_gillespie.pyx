from random import random
from math import log

def activ(x,k,n):
    x=(x/k)**n
    return x/(1+x)
def repress(x,k,n):
    x=(x/k)**n
    return 1/(1+x)

def update_rates(conc,rates):
    # rates = [0]*6
    rates[0] = 100*activ(conc[2],50,2)*repress(conc[1],50,2)
    rates[1] = 100*activ(conc[0],50,2)*repress(conc[2],50,2)
    rates[2] = 100*activ(conc[1],50,2)*repress(conc[0],50,2)
    rates[3] = 0.005*conc[0]
    rates[4] = 0.005*conc[1]
    rates[5] = 0.005*conc[2]
    return rates

def run_gillespie():
    
    cdef int stoichiometry[6][3]    
    stoichiometry = [[1,0,0],[0,1,0],[0,0,1],[-1,0,0],[0,-1,0],[0,0,-1]]
    cdef int conc[3]    
    conc = [100,50,50]
    cdef float rates[6]
    rates = [0.0]*6    
    cdef float t = 0
    cdef float dt = 0.0
    cdef float a0 = 0.0
    cdef float tot = 0.0
    cdef float rand1 = 0.0
    cdef float rand2 = 0.0
    cdef float tMax = 1000
    
    while t < tMax:
        rates = update_rates(conc,rates)
        a0 = sum(rates)
        rand1 = random()
        dt = 1/a0*log(1/rand1)
        rand2 = random()
        tot = 0
        for rindex in range(len(rates)):
            tot+= rates[rindex]/a0
            if rand2<tot:
                break
        for i in range(len(conc)):
            conc[i] += stoichiometry[rindex][i]
            if(conc[i]<0):
                conc[i]=0
        t+=dt
        # times.append(t)
        # results.append(list(conc))
    
