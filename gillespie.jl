function activ(x::Int64,k::Float64,n::Float64)
    x = (x/k)^n
    return x/(1+x)
end

function repress(x::Int64,k::Float64,n::Float64)
    x = (x/k)^n
    return 1/(1+x)
end

function update_rates(conc::Array{Int64,2},rates::Array{Float64,1})
    rates[1] = 100*activ(conc[3],50.0,2.0)*repress(conc[2],50.0,2.0)
    rates[2] = 100*activ(conc[1],50.0,2.0)*repress(conc[3],50.0,2.0)
    rates[3] = 100*activ(conc[2],50.0,2.0)*repress(conc[1],50.0,2.0)
    rates[4] = 0.005*conc[1]
    rates[5] = 0.005*conc[2]
    rates[6] = 0.005*conc[3]
end

function run_gillespie(conc = [100 50 50])
    stoichiometry::Array{Int64,2} = [1 0 0;0 1 0;0 0 1;-1 0 0;0 -1 0;0 0 -1]
    rates::Array{Float64,1} = zeros(6)
    results = Matrix{Float64}(0,3)
    t::Float64 = 0
    tMax::Float64 = 1000
    times::Array{Float64,1} = [t]
    rindex::Int64 = 0
    tot::Float64 = 0
    while t < tMax
        update_rates(conc,rates)
        a0 = sum(rates)
        rand1 = rand()
        dt = 1/a0*log(1/rand1)
        rand2 = rand()
        tot = 0
        for i=1:length(rates)
            tot+= rates[i]/a0
            if rand2<tot
                rindex=i
                break
            end
        end
        for i=1:length(conc)
            conc[i] += stoichiometry[rindex,i]
            conc[i] = max(0,conc[i])
        end 
        t+=dt
    end
    return times , results
end

function main()
    tic()
    times,results = run_gillespie()
    time1 = toq()
    tic()
    times,results = run_gillespie([130 70 80])
    time2 = toq()
    @printf("First execution julia =  %f\n",time1)
    @printf("Second execution julia = %f\n",time2)
end

main()
