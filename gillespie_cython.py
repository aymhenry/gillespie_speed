from lib_run_gillespie import run_gillespie
import time
if __name__ == "__main__":
    t1 = time.clock()
    run_gillespie()
    t2 = time.clock()
    print("Time cython =\t\t %.6f"%(t2-t1))
