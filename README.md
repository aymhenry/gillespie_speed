# Speed Comparison

This little project compares the seed of the same Gillespie algorithm coded in different languages.

**Note:** In every program, only the function called `run_gillespie` is timed.

- `gillespie.py` - Basic python
- `gillespie_cython.py` and `lib_run_gillespie.pyx` - Cython library
- `gillespie_numba.py` - Tries the numba python library
- `gillespie.c` - C
- `gillespie.c` - C++
- `gillespie.jl` - julia

## Dependencies 
- a `gcc` C compiler
- the `g++` C++ compiler
- julia installed (and visible in the PATH)
- cython installed
- the numba library installed

## Execution

To execute the programs, use the `make` in a shell:

```bash
make run
```
