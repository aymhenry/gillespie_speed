
run:gillespiec gillespiecpp lib_run_gillespie
	@python3 gillespie.py
	@python3 gillespie_numba.py
	@python3 gillespie_cython.py
	@./gillespiec
	@./gillespiecpp
	@julia gillespie.jl

gillespiec:gillespie.c
	gcc gillespie.c -o gillespiec -lm
gillespiecpp:gillespie.cpp
	g++ gillespie.cpp -o gillespiecpp -lm
lib_run_gillespie:lib_run_gillespie.pyx setup.py
	python setup.py build_ext --inplace
clean:
	rm gillespiec gillespiecpp lib_run_gillespie.c lib_run_gillespie.cpython*
